<?php
session_start();
?>

<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                Günlük Çalışma Saatleri</div>
            <div class="alert alert-success" style="display:none;">
                <span class="glyphicon glyphicon-ok"></span> Drag table row and cange Order</div>
            <table class="table">
                <thead>
                <tr>
               <?php for($i=1;$i<=$_SESSION['satirsayisi'];$i++) {?>
                        <th>
                        <?php echo $_SESSION['s'.$i.'_adi']; ?>
                        </th>
               <?php }    ?>
                    </tr>
                </thead>
                <tbody>
            
                    <tr>
                    <?php for($i=1;$i<=$_SESSION['satirsayisi'];$i++) {?>
                        <td>
                        <?php echo $_SESSION['s'.$i]; ?>
                        </td>
                        <?php }?>
                      
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
