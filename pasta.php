<?php 
session_start();
header("content-type:image/gif"); // gif formatında çalışacağımız belirtiyoruz 
$pasta=array($_SESSION['s1'],$_SESSION['s2'],$_SESSION['s3'],$_SESSION['s4'],$_SESSION['s5']); // pasta dilimi parçalarımız 
$veri=array($_SESSION['s1_adi'],$_SESSION['s2_adi'],$_SESSION['s3_adi'],$_SESSION['s4_adi'],$_SESSION['s5_adi']); // pasta dilimi parçalarımız 
$toplam=array_sum($pasta); // pasta dilimi parçalarının toplamı , bize %lik dilim hesaplarken lazım olacak 
$img=imagecreate(400,200); // 420 px genişliğinde 240px yükseliğinde tuval oluşturuyoruz 
$bg=imagecolorallocate($img,0,0,0); // tuvalimizi siyah rengine boyuyoruz 
$width=200; 
$height=200; 
$cx=$width/2; 
$cy=$height/2; 
$start=0; 
for($i=0;$i<count($pasta);$i++){ 
$e=round(($pasta[$i]/$toplam)*100,2); 
$end=round($e*360/100); // pasta dilimi için açı değeri 
$rgb1=$i*400+50; 
$rgb2=$i*200+120;
$rgb3=$i*100+120;
$renk=imagecolorallocate($img,$rgb1,$rgb2,$rgb3); 
imagefilledarc($img,$cx,$cy,$width,$height,$start,$start+$end,$renk,IMG_ARC_PIE); 
$start+=$end; 
imagefilledrectangle($img,230,10+$i*30,250,30+$i*30,$renk); 
imagestring($img,2,256,12+$i*30,$veri[$i].' %'.$e.' ',$renk); 
} 
imagegif($img); 
imagedestroy($img); 
?>